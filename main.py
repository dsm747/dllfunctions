import inspect
from DLLS.DLLFunctions import E1


def execute_functions(all_functions):
    list_json = []
    for key, value in all_functions:
        if str(inspect.signature(value)) == "()":
            t = value()
            list_json.append(t)
    return list_json


if __name__ == '__main__':
    all_func = inspect.getmembers(E1, inspect.isfunction)
    func_engine = []

    for i in execute_functions(all_func):
        func_engine.append(i)

    print(str(func_engine))
