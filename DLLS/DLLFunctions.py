import datetime
from ctypes import *
import os
from re import T
from DLLS.DLLControll import DLL
from decorators import command_block
import base64
import platform

XML_SAIDA_VENDA_SAT_DESENVOLVIMENTO = "<CFe><infCFe Id=\"CFe35170900246872000134590002121801051011580881\" versao=\"0.07\" versaoDadosEnt=\"0.07\" versaoSB=\"010100\"><ide><cUF>35</cUF><cNF>158088</cNF><mod>59</mod><nserieSAT>000212180</nserieSAT><nCFe>105101</nCFe><dEmi>20170919</dEmi><hEmi>201653</hEmi><cDV>1</cDV><tpAmb>1</tpAmb><CNPJ>03995946000123</CNPJ><signAC>NqtMtUKXugLtXR7knqixrLCDXLtIssXu/ZhY7hJw0ixDiJ+rpS3uM9SoF6dFQOX6DnNrIyV5+Poa3JhIz+tIL6Cl2vWiFdSTwg4Gl7qRFh3jWnbNxsVCAP33iXSNJP82aVVJNOtDlP4iBJxqa7JtS9C5gv1csqCvV5j7Pwk81uiaZHYptZPByQQbkhG6hZ+wcRdV3Fh24RfkpomyBJJqlGvD1+9kQHvJzgJ3jIRKYQyh7ZugqmVBB3BJ2ML9mSrS5fPzKtI6TgIumlBghAXw8Tbqbo3hkEqym4tbNZcNUlksAb98/jS1Rjiz1yKgGQB/VCN0JoD9YGW2i9XUjVq45Q==</signAC><assinaturaQRCODE>lAcssLXWsnzF3hxNlqrdr8SLOCrfqmONBXlKBTXEjDsb6rDsYngAiZF+3QdCNaZr7qopgmD3CbJ72unXikS9LQE+6bRYQUCN8651mZ0416pKbpwNyRxYCEcuv4JFYAYM6vasp/NikdSfB6CCTVhZWP0EAKFQwJ8p5UUWL6Z8C4+GDMG/MB4GYoolaK86YaNj4KK2ZQvzoip1CQPMQyi89qJRd1TOQrBqVTLbTVtz9h3UUheZTtUzkFcJD8LJpb+f/tSeNrsdgcDUTFP4f4vrXUekCqGkGPqoX/AfDp0zOtMWwUmk1zb6e233aupeLlMidobVR2fcx8YXKMxoXQwp8g==</assinaturaQRCODE><numeroCaixa>201</numeroCaixa></ide><emit><CNPJ>00246872000134</CNPJ><xNome>ODALTIR DE MEDEIROS e CIA LTDA</xNome><xFant>SUPERMERCADO MEDEIROS</xFant><enderEmit><xLgr>RUA SAO PAULO</xLgr><nro>1843</nro><xBairro>CENTRO</xBairro><xMun>SAO JOAQUIM DA BARRA</xMun><CEP>14600000</CEP></enderEmit><IE>642001126110</IE><IM>5317</IM><cRegTrib>3</cRegTrib><indRatISSQN>S</indRatISSQN></emit><dest/><det nItem=\"1\"><prod><cProd>1038</cProd><xProd>LARANJA PERA kg</xProd><NCM>08051000</NCM><CFOP>5102</CFOP><uCom>KG</uCom><qCom>1.4900</qCom><vUnCom>1.295</vUnCom><vProd>1.92</vProd><indRegra>A</indRegra><vItem>1.92</vItem></prod><imposto><ICMS><ICMS40><Orig>0</Orig><CST>40</CST></ICMS40></ICMS><PIS><PISNT><CST>06</CST></PISNT></PIS><COFINS><COFINSNT><CST>06</CST></COFINSNT></COFINS></imposto></det><det nItem=\"2\"><prod><cProd>7898930673134</cProd><cEAN>7898930673134</cEAN><xProd>SAKKOS 3KG 50un</xProd><NCM>39232110</NCM><CFOP>5102</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>6.950</vUnCom><vProd>6.95</vProd><indRegra>A</indRegra><vItem>6.95</vItem></prod><imposto><vItem12741>1.96</vItem12741><ICMS><ICMS00><Orig>0</Orig><CST>00</CST><pICMS>18.00</pICMS><vICMS>1.25</vICMS></ICMS00></ICMS><PIS><PISAliq><CST>01</CST><vBC>6.95</vBC><pPIS>0.0165</pPIS><vPIS>0.11</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>6.95</vBC><pCOFINS>0.0760</pCOFINS><vCOFINS>0.53</vCOFINS></COFINSAliq></COFINS></imposto></det><det nItem=\"3\"><prod><cProd>7891150001954</cProd><cEAN>7891150001954</cEAN><xProd>ADES ORIG. ZERO 1L</xProd><NCM>22029900</NCM><CFOP>5405</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>7.980</vUnCom><vProd>7.98</vProd><indRegra>A</indRegra><vItem>7.98</vItem><obsFiscoDet xCampoDet=\"Cod. CEST\"><xTextoDet>0302000</xTextoDet></obsFiscoDet></prod><imposto><ICMS><ICMS40><Orig>0</Orig><CST>60</CST></ICMS40></ICMS><PIS><PISAliq><CST>01</CST><vBC>7.98</vBC><pPIS>0.0165</pPIS><vPIS>0.13</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>7.98</vBC><pCOFINS>0.0760</pCOFINS><vCOFINS>0.61</vCOFINS></COFINSAliq></COFINS></imposto></det><det nItem=\"4\"><prod><cProd>7891150001954</cProd><cEAN>7891150001954</cEAN><xProd>ADES ORIG. ZERO 1L</xProd><NCM>22029900</NCM><CFOP>5405</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>7.980</vUnCom><vProd>7.98</vProd><indRegra>A</indRegra><vItem>7.98</vItem><obsFiscoDet xCampoDet=\"Cod. CEST\"><xTextoDet>0302000</xTextoDet></obsFiscoDet></prod><imposto><ICMS><ICMS40><Orig>0</Orig><CST>60</CST></ICMS40></ICMS><PIS><PISAliq><CST>01</CST><vBC>7.98</vBC><pPIS>0.0165</pPIS><vPIS>0.13</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>7.98</vBC><pCOFINS>0.0760</pCOFINS><vCOFINS>0.61</vCOFINS></COFINSAliq></COFINS></imposto></det><det nItem=\"5\"><prod><cProd>7896200116114</cProd><cEAN>7896200116114</cEAN><xProd>AZ.BALTICO SUAV500ml</xProd><NCM>21039021</NCM><CFOP>5405</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>12.900</vUnCom><vProd>12.90</vProd><indRegra>A</indRegra><vItem>12.90</vItem><obsFiscoDet xCampoDet=\"Cod. CEST\"><xTextoDet>1703500</xTextoDet></obsFiscoDet></prod><imposto><vItem12741>4.05</vItem12741><ICMS><ICMS40><Orig>0</Orig><CST>60</CST></ICMS40></ICMS><PIS><PISAliq><CST>01</CST><vBC>12.90</vBC><pPIS>0.0165</pPIS><vPIS>0.21</vPIS></PISAliq></PIS><COFINS><COFINSAliq><CST>01</CST><vBC>12.90</vBC><pCOFINS>0.0760</pCOFINS><vCOFINS>0.98</vCOFINS></COFINSAliq></COFINS></imposto></det><total><ICMSTot><vICMS>1.25</vICMS><vProd>37.73</vProd><vDesc>0.00</vDesc><vPIS>0.58</vPIS><vCOFINS>2.73</vCOFINS><vPISST>0.00</vPISST><vCOFINSST>0.00</vCOFINSST><vOutro>0.00</vOutro></ICMSTot><vCFe>37.73</vCFe><vCFeLei12741>6.01</vCFeLei12741></total><pgto><MP><cMP>01</cMP><vMP>37.73</vMP></MP><vTroco>0.00</vTroco></pgto></infCFe><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"#CFe35170900246872000134590002121801051011580881\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>5pioY3cH5RZt9sySj8n+xTOZS8Kd/tRlTdRNrNYKbK4=</DigestValue></Reference></SignedInfo><SignatureValue>1LB2Ce7P+XuikzBxQIsUJ4xC/CmloQ9WgQyI1kMth2Vz+qUcaAlXhFIxFHWrZUi3tx1qp5n5MG4+A3nc80Mj3OuzRI9gx8lSDpBaqNu2uyDKEPFEizvjlEavCyxMFKqYO5anfmUO4O3pz3CITX0KYXHP5EHeFHShdPq9GcedtZvXkt4A7abUkt9J0BlCa/k1PGzryVPjIfNygdfyXtcgu0W8SpbL8VVYJQ/unTtjKyMhF98XH4GLHM7NyUT/5DexWNecd/uPaXISU66ioPqHvs44LYRwyFqHPI59JHiVsmTY6t5vRp3yzguxMQH90IlSQQg8aZsPLuwh1jRmAyQq3g==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIGgjCCBGqgAwIBAgIJASIsy2U/i7b1MA0GCSqGSIb3DQEBCwUAMFExNTAzBgNVBAoTLFNlY3JldGFyaWEgZGEgRmF6ZW5kYSBkbyBFc3RhZG8gZGUgU2FvIFBhdWxvMRgwFgYDVQQDEw9BQyBTQVQgU0VGQVogU1AwHhcNMTYxMTMwMTg0NTA2WhcNMjExMTMwMTg0NTA2WjCBuzESMBAGA1UEBRMJMDAwMjEyMTgwMQswCQYDVQQGEwJCUjESMBAGA1UECBMJU2FvIFBhdWxvMREwDwYDVQQKEwhTRUZBWi1TUDEPMA0GA1UECxMGQUMtU0FUMSgwJgYDVQQLEx9BdXRlbnRpY2FkbyBwb3IgQVIgU0VGQVogU1AgU0FUMTYwNAYDVQQDEy1PREFMVElSIERFIE1FREVJUk9TIGUgQ0lBIExUREE6MDAyNDY4NzIwMDAxMzQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDcOTDLK0KO8ue7soiaNEPz4UpsZB5DJklFZGyMZXqSYOxyLLsdjMBcMCTqDy/M/fGNaXu59SlyBgUQkUmaRY1kmgE5Sfp6DtV5IdjopEZ2CgbUmdA3in0o113PHjMS4GUzOgaXHiviGnU8bN2f/DmyJdYBnxk7rWsWU3xhvrkgsMFLUPNfYEhjrwGnUejD+rFiBNzE7oQkNQ4YZftfbZAkTJWRUEzpYfV7F7tfhFnQGHU+3eFMsd1E0xO0Z9VEoBvQmRvr9MGifYko2+XPn/jXnpeoxDOaTkLjpkmErfxU8KsKmzE4LAx3WNCUTpc1FgcO5rpbGXYaEzgM/3yjc5NrAgMBAAGjggHwMIIB7DAOBgNVHQ8BAf8EBAMCBeAwdQYDVR0gBG4wbDBqBgkrBgEEAYHsLQMwXTBbBggrBgEFBQcCARZPaHR0cDovL2Fjc2F0LmltcHJlbnNhb2ZpY2lhbC5jb20uYnIvcmVwb3NpdG9yaW8vZHBjL2Fjc2VmYXpzcC9kcGNfYWNzZWZhenNwLnBkZjBlBgNVHR8EXjBcMFqgWKBWhlRodHRwOi8vYWNzYXQuaW1wcmVuc2FvZmljaWFsLmNvbS5ici9yZXBvc2l0b3Jpby9sY3IvYWNzYXRzZWZhenNwL2Fjc2F0c2VmYXpzcGNybC5jcmwwgZQGCCsGAQUFBwEBBIGHMIGEMC4GCCsGAQUFBzABhiJodHRwOi8vb2NzcC5pbXByZW5zYW9maWNpYWwuY29tLmJyMFIGCCsGAQUFBzAChkZodHRwOi8vYWNzYXQuaW1wcmVuc2FvZmljaWFsLmNvbS5ici9yZXBvc2l0b3Jpby9jZXJ0aWZpY2Fkb3MvYWNzYXQucDdjMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAkGA1UdEwQCMAAwJAYDVR0RBB0wG6AZBgVgTAEDA6AQBA4wMDI0Njg3MjAwMDEzNDAfBgNVHSMEGDAWgBSwhYGzKM12KikkS19YSm9o2aywKjANBgkqhkiG9w0BAQsFAAOCAgEAor5yDjSp4bnQcg5q+bj2dlUCWTWjUsLI/H+9eq5Ej+mPKktPLL1DSrX9Rvu8hj3bVADK1zo/vK0irToiurWQln27OoO3GcXdNmNcdYRMIvcWYTIsU//cVYWcXtww65Rsll8yBR0/S+z/hsN+CZ2uL3u4V+VC+eOgVhmbbs+8/atOL53UMT6nDNSnkFzEt4xkiNLVfaaRU0pNrjYkMvd/L+xAHsNTrqpGy6yiRRfRUygIuyxsqM9VmtoBPizIuVcYPBF7kq9Sez3QJlo8cS5VTfy1q/7ZsIiRSr5O4r9ezlDKHIdkiqaHdDcrr9m7tij+Ix790+7OFoV/CdWwcCvjaAAJHGC1caijWMiZY6Qo4RTPECu14cr9ocGUzCfhQM3j8gSOIQoy2UrxuSLRgWPX3mjuvczukZxOy9JkalcAhTxckNjVKaULt1OVrJKbb+K8lxyuPGCYaRcWZnaDs5o7Iob/fc/fmxnzE7gr+/K40lZtUDI32bl/EKyuXWc8zvbjsyN4x1skmd6PCFhxsI8q9QkmJOs/4DlkmPgku3BX1OtlFwMmvjfR5B5io6pm6rSr9k0yhMzgjEMNaBpSkY3oMjjdUOpIUvXU2ImimYfA43PDonXBQh74Tw7+fpMC+fB5ii9dWHc+NLUDHuF9LqPE2dhNOIvreN4CAbSmOmceg3g=</X509Certificate></X509Data></KeyInfo></Signature></CFe>"
XML_SAIDA_CANCELAMENTO_SAT_DESENVOLVIMENTO = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><CFeCanc><infCFe Id=\"CFe13180314200166000166599000108160001324252883\" chCanc=\"CFe13180314200166000166599000108160001316693175\" versao=\"0.07\"><dEmi>20180305</dEmi><hEmi>142819</hEmi><ide><cUF>13</cUF><cNF>425288</cNF><mod>59</mod><nserieSAT>900010816</nserieSAT><nCFe>000132</nCFe><dEmi>20180305</dEmi><hEmi>142846</hEmi><cDV>3</cDV><CNPJ>16716114000172</CNPJ><signAC>SGR-SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT</signAC><assinaturaQRCODE>Q5DLkpdRijIRGY6YSSNsTWK1TztHL1vD0V1Jc4spo/CEUqICEb9SFy82ym8EhBRZjbh3btsZhF+sjHqEMR159i4agru9x6KsepK/q0E2e5xlU5cv3m1woYfgHyOkWDNcSdMsS6bBh2Bpq6s89yJ9Q6qh/J8YHi306ce9Tqb/drKvN2XdE5noRSS32TAWuaQEVd7u+TrvXlOQsE3fHR1D5f1saUwQLPSdIv01NF6Ny7jZwjCwv1uNDgGZONJdlTJ6p0ccqnZvuE70aHOI09elpjEO6Cd+orI7XHHrFCwhFhAcbalc+ZfO5b/+vkyAHS6CYVFCDtYR9Hi5qgdk31v23w==</assinaturaQRCODE><numeroCaixa>001</numeroCaixa></ide><emit><CNPJ>14200166000166</CNPJ><xNome>ELGIN INDUSTRIAL DA AMAZONIA LTDA</xNome><enderEmit><xLgr>AVENIDA ABIURANA</xLgr><nro>579</nro><xBairro>DIST INDUSTRIAL</xBairro><xMun>MANAUS</xMun><CEP>69075010</CEP></enderEmit><IE>111111111111</IE><IM>111111</IM></emit><dest><CPF>14808815893</CPF></dest><total><vCFe>3.00</vCFe></total><infAdic><obsFisco xCampo=\"xCampo1\"><xTexto>xTexto1</xTexto></obsFisco></infAdic></infCFe><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256\"/><Reference URI=\"#CFe13180314200166000166599000108160001324252883\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2001/04/xmlenc#sha256\"/><DigestValue>pePcOYfIU+b59qGayJiJj492D9fTVhqbHEqFLDUi1Wc=</DigestValue></Reference></SignedInfo><SignatureValue>og35vHuErSOCB29ME4WRwdVPwps/mOUQJvk3nA4Oy//CVPIt0X/iGUZHMnJhQa4aS4c7dq5YUaE2yf8H9FY8xPkY9vDQW62ZzuM/6qSHeh9Ft09iP55T76h7iLY+QLl9FZL4WINmCikv/kzmCCi4+8miVwx1MnFiTNsgSMmzRnvAv1iVkhBogbAZES03iQIi7wZGzZDo7bFmWyXVdtNnjOke0WS0gTLhJbftpDT3gi0Muu8J+AfNjaziBMFQB3i1oN96EkpCKsT78o5Sb+uBux/bV3r79nrFk4MXzaFOgBoTqv1HF5RVNx2nWSoZrbpAV8zPB1icnAnfb4Qfh1oJdA==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIFzTCCBLWgAwIBAgICESswDQYJKoZIhvcNAQENBQAwaDELMAkGA1UEBhMCQlIxEjAQBgNVBAgMCVNBTyBQQVVMTzESMBAGA1UEBwwJU0FPIFBBVUxPMQ8wDQYDVQQKDAZBQ0ZVU1AxDzANBgNVBAsMBkFDRlVTUDEPMA0GA1UEAwwGQUNGVVNQMB4XDTE3MDEyNzEzMzMyMloXDTIyMDEyNjEzMzMyMlowgbIxCzAJBgNVBAYTAkJSMREwDwYDVQQIDAhBbWF6b25hczERMA8GA1UECgwIU0VGQVotU1AxGDAWBgNVBAsMD0FDIFNBVCBTRUZBWiBTUDEoMCYGA1UECwwfQXV0b3JpZGFkZSBkZSBSZWdpc3RybyBTRUZBWiBTUDE5MDcGA1UEAwwwRUxHSU4gSU5EVVNUUklBTCBEQSBBTUFaT05JQSBMVERBOjE0MjAwMTY2MDAwMTY2MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtyLG8URyX8fqjOQa+rj3Rl6Z6eIX/dndhNe0rw6inNAXt06HtXQslBqnReuSanN3ssgpV6oev0ikfXA7hhmpZM7qVigTJp3+h1K9vKUlPZ5ELT36yAokpxakIyYRy5ELjP4KwFrAjQUgB6xu5X/MOoUmBKRLIiwm3wh7kUA9jZArQGD4pRknuvFuQ99ot3y6u3lI7Oa2ZqJ1P2E7NBmfdswQL8VG51by0Weivugsv3xWAHvdXZmmOrmv2W5C2U2VnsTjA3p2zQVwitZBEh6JxqLE3KljXlokbhHb1m2moSbzRLCdAJHIq/6eWL8kl2OVWViECODGoYA0Qz0wSgk/vwIDAQABo4ICNDCCAjAwCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBeAwLAYJYIZIAYb4QgENBB8WHU9wZW5TU0wgR2VuZXJhdGVkIENlcnRpZmljYXRlMB0GA1UdDgQWBBTIeTKrUS19raxSgeeIHYSXclNYkDAfBgNVHSMEGDAWgBQVtOORhiQs6jNPBR4tL5O3SJfHeDATBgNVHSUEDDAKBggrBgEFBQcDAjBDBgNVHR8EPDA6MDigNqA0hjJodHRwOi8vYWNzYXQuZmF6ZW5kYS5zcC5nb3YuYnIvYWNzYXRzZWZhenNwY3JsLmNybDCBpwYIKwYBBQUHAQEEgZowgZcwNQYIKwYBBQUHMAGGKWh0dHA6Ly9vY3NwLXBpbG90LmltcHJlbnNhb2ZpY2lhbC5jb20uYnIvMF4GCCsGAQUFBzAChlJodHRwOi8vYWNzYXQtdGVzdGUuaW1wcmVuc2FvZmljaWFsLmNvbS5ici9yZXBvc2l0b3Jpby9jZXJ0aWZpY2Fkb3MvYWNzYXQtdGVzdGUucDdjMHsGA1UdIAR0MHIwcAYJKwYBBAGB7C0DMGMwYQYIKwYBBQUHAgEWVWh0dHA6Ly9hY3NhdC5pbXByZW5zYW9maWNpYWwuY29tLmJyL3JlcG9zaXRvcmlvL2RwYy9hY3NhdHNlZmF6c3AvZHBjX2Fjc2F0c2VmYXpzcC5wZGYwJAYDVR0RBB0wG6AZBgVgTAEDA6AQDA4xNDIwMDE2NjAwMDE2NjANBgkqhkiG9w0BAQ0FAAOCAQEAAhF7TLbDABp5MH0qTDWA73xEPt20Ohw28gnqdhUsQAII2gGSLt7D+0hvtr7X8K8gDS0hfEkv34sZ+YS9nuLQ7S1LbKGRUymphUZhAfOomYvGS56RIG3NMKnjLIxAPOHiuzauX1b/OwDRmHThgPVF4s+JZYt6tQLESEWtIjKadIr4ozUXI2AcWJZL1cKc/NI7Vx7l6Ji/66f8l4Qx704evTqN+PjzZbFNFvbdCeC3H3fKhVSj/75tmK2TBnqzdc6e1hrjwqQuxNCopUSV1EJSiW/LR+t3kfSoIuQCPhaiccJdAUMIqethyyfo0ie7oQSn9IfSms8aI4lh2BYNR1mf5w==</X509Certificate></X509Data></KeyInfo></Signature></CFeCanc>"
ASSQRCODE_ = "blcamrPugGfK1VV8ixHUyfgh66vhzH7hVURbW1BaHR4eBHHtWBaZmcUuxSdpkaSl8WZ9umJkhJ4JXfKCAxJrJSRqQ6ZPpdFvEEnInJGbZ71mnM0ofTsN4+kJwem8Ky/nagqHSrZnVP3mr3L4j8c5eaiarSqu4tjgXSNnbhfntwhB8RNTxx1irsTZeJmqFOeQJ+XSkkW+ScWbGMw+gRlsI2ZTJgxaaTha1v6cltDscdeKuCYfVgu5U+WqffbAZrb9mw71rY4ccARpHo8c7qn+dEknS5f5LSfnm7llosFhlOwK+wvFKV47PRvrIHZStUxPZSw2F5FPq+IRtfvrB2EAuA=="

# Imprime XML NFCe
XML_NFCE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><nfeProc versao=\"4.00\" xmlns=\"http://www.portalfiscal.inf.br/nfe\"><NFe xmlns=\"http://www.portalfiscal.inf.br/nfe\"><infNFe versao=\"4.00\" Id=\"NFe33180705481336000137651000000916401000005909\"><ide><cUF>33</cUF><cNF>00000590</cNF><natOp>VENDA CONSUMIDOR</natOp><mod>65</mod><serie>100</serie><nNF>91640</nNF><dhEmi>2018-07-03T15:03:09-03:00</dhEmi><tpNF>1</tpNF><idDest>1</idDest><cMunFG>3304557</cMunFG><tpImp>4</tpImp><tpEmis>1</tpEmis><cDV>9</cDV><tpAmb>2</tpAmb><finNFe>1</finNFe><indFinal>1</indFinal><indPres>1</indPres><procEmi>0</procEmi><verProc>DJPDV 1.5.4</verProc></ide><emit><CNPJ>00000000000000</CNPJ><xNome>Automacao Comercial</xNome><xFant>SYSTEM</xFant><enderEmit><xLgr>Avenida ABC</xLgr><nro>70</nro><xBairro>Centro</xBairro><cMun>3304557</cMun><xMun>Rio de Janeiro</xMun><UF>RJ</UF><CEP>20071001</CEP><cPais>1058</cPais><xPais>BRASIL</xPais><fone>1133835927</fone></enderEmit><IE>11111111</IE><IM>0000000</IM><CRT>1</CRT></emit><det nItem=\"1\"><prod><cProd>1</cProd><cEAN>SEM GTIN</cEAN><xProd>NOTA FISCAL EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL</xProd><NCM>01022911</NCM><cBenef>1234567890</cBenef><CFOP>5102</CFOP><uCom>UN</uCom><qCom>10.0000</qCom><vUnCom>1.0000000000</vUnCom><vProd>10.00</vProd><cEANTrib>SEM GTIN</cEANTrib><uTrib>UN</uTrib><qTrib>10.0000</qTrib><vUnTrib>1.0000000000</vUnTrib><indTot>1</indTot></prod><imposto><vTotTrib>1.62</vTotTrib><ICMS><ICMSSN102><orig>0</orig><CSOSN>102</CSOSN></ICMSSN102></ICMS><PIS><PISOutr><CST>49</CST><vBC>0.00</vBC><pPIS>0.0000</pPIS><vPIS>0.00</vPIS></PISOutr></PIS><COFINS><COFINSOutr><CST>49</CST><vBC>0.00</vBC><pCOFINS>0.0000</pCOFINS><vCOFINS>0.00</vCOFINS></COFINSOutr></COFINS></imposto><infAdProd>TESTE DE COMPLEMENTO A SER IMPRESSO NO CFE, COM PRODUTO COM DESCRICAO DE 120 CARACTERES</infAdProd></det><total><ICMSTot><vBC>0.00</vBC><vICMS>0.00</vICMS><vICMSDeson>0.00</vICMSDeson><vFCP>0.00</vFCP><vBCST>0.00</vBCST><vST>0.00</vST><vFCPST>0.00</vFCPST><vFCPSTRet>0.00</vFCPSTRet><vProd>10.00</vProd><vFrete>0.00</vFrete><vSeg>0.00</vSeg><vDesc>0.00</vDesc><vII>0.00</vII><vIPI>0.00</vIPI><vIPIDevol>0.00</vIPIDevol><vPIS>0.00</vPIS><vCOFINS>0.00</vCOFINS><vOutro>0.00</vOutro><vNF>10.00</vNF><vTotTrib>1.62</vTotTrib></ICMSTot></total><transp><modFrete>9</modFrete></transp><pag><detPag><tPag>03</tPag><vPag>2.01</vPag><card><tpIntegra>1</tpIntegra><CNPJ>84477152000170</CNPJ><tBand>01</tBand><cAut>000030034</cAut></card></detPag><detPag><tPag>02</tPag><vPag>0.99</vPag></detPag><detPag><tPag>01</tPag><vPag>3.00</vPag></detPag><detPag><tPag>04</tPag><vPag>4.00</vPag><card><tpIntegra>2</tpIntegra></card></detPag></pag><infAdic><infCpl>Trib aprox R$:0,42 Federal, 1,20 Estadual;Fonte:IBPT F3W1D7;</infCpl></infAdic></infNFe><Signature xmlns=\"http://www.w3.org/2000/09/xmldsig#\"><SignedInfo><CanonicalizationMethod Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/><SignatureMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#rsa-sha1\"/><Reference URI=\"#NFe33180705481336000137651000000916401000005909\"><Transforms><Transform Algorithm=\"http://www.w3.org/2000/09/xmldsig#enveloped-signature\"/><Transform Algorithm=\"http://www.w3.org/TR/2001/REC-xml-c14n-20010315\"/></Transforms><DigestMethod Algorithm=\"http://www.w3.org/2000/09/xmldsig#sha1\"/><DigestValue>7kCR1M/dm/DQM0WmQyXSLj7r6P4=</DigestValue></Reference></SignedInfo><SignatureValue>ycRA7XNdUmn401+kTovSXJtSh9+a+umgcJX92n+b0Zged2+qIjsv002RGK8fb8/3w5ypKQahsbl6lf/KV68UL8DXQW1SWt1Fqc6jDMg86IahOfMAeCVDGKY36kFH80QwZLyTnDq8Zmzi7cul+tJC+gIBtE2QMekO/GTpRlF6R0dDxswGcySJY3yUuitz/oMP8ZXNZGmaQemUSSeTNL4+5vAngzobc8ATu8WbC5T7q5G6GAwoXCzxsykZl46zezfcV1iymWn8KEw97aYrudDE8njklMyqeiFTb1aK7FQQ/Ru9hN8pkf8Pllc7KjMrnB+ApWhmNfxfJoNQax54hztDeg==</SignatureValue><KeyInfo><X509Data><X509Certificate>MIIIITCCBgmgAwIBAgIIBRRT4LGdengwDQYJKoZIhvcNAQELBQAwcjELMAkGA1UEBhMCQlIxEzARBgNVBAoTCklDUC1CcmFzaWwxNjA0BgNVBAsTLVNlY3JldGFyaWEgZGEgUmVjZWl0YSBGZWRlcmFsIGRvIEJyYXNpbCAtIFJGQjEWMBQGA1UEAxMNQUMgT05MSU5FIFJGQjAeFw0xODA0MTIxNzEyMjZaFw0xOTA0MTIxNzEyMjZaMIHoMQswCQYDVQQGEwJCUjELMAkGA1UECAwCU1AxDjAMBgNVBAcMBVRBVFVJMRMwEQYDVQQKDApJQ1AtQnJhc2lsMTYwNAYDVQQLDC1TZWNyZXRhcmlhIGRhIFJlY2VpdGEgRmVkZXJhbCBkbyBCcmFzaWwgLSBSRkIxFjAUBgNVBAsMDVJGQiBlLUNOUEogQTExITAfBgNVBAsMGEFSIENPTkVDVElWSURBREUgRElHSVRBTDE0MDIGA1UEAwwrRCBKIEFVVE9NQUNBTyBDT01FUkNJQUwgTFREQTowNTQ4MTMzNjAwMDEzNzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBANIp47mMNpUMOs+mIFE0ePbu9mOmlYf1XtKYB0u9SDb6ZXCIDDaTSGY5wXOroplv5D3thsjh2Jx913lSKYKUf5fz7gTWDrIRLzIg7eOVfrdm4U8I/JVvsXb0K8V7m0vhqocUGMoe/9LplQBT9kHd0aDLZtIE61q90yJsbbYi8O2qc7siy3NWTdhP+QfF8CXCfqDhtqZYkfRObKIpUJMO0tT5CuAQw7eQpNhsxztduphRAYNVLU5euplqnllAkGPrL5D0WjlYfVCTiE/kdHyZj+1x80EItKwkNooYQHotpi5RKe/M40EaZKC/9QHE9ylW2DZEvu0PHorUSREhZOXUv7UCAwEAAaOCA0IwggM+MIGhBggrBgEFBQcBAQSBlDCBkTBcBggrBgEFBQcwAoZQaHR0cDovL2ljcC1icmFzaWwudnBraS52YWxpZGNlcnRpZmljYWRvcmEuY29tLmJyL2FjLW9ubGluZXJmYi9hYy1vbmxpbmVyZmJ2Mi5wN2IwMQYIKwYBBQUHMAGGJWh0dHA6Ly9vY3NwLnZhbGlkY2VydGlmaWNhZG9yYS5jb20uYnIwCQYDVR0TBAIwADAfBgNVHSMEGDAWgBSRmnaMK6iTGJiYegPky+y1sBkn/zB1BgNVHSAEbjBsMGoGBmBMAQIBNzBgMF4GCCsGAQUFBwIBFlJodHRwOi8vaWNwLWJyYXNpbC52cGtpLnZhbGlkY2VydGlmaWNhZG9yYS5jb20uYnIvYWMtb25saW5lcmZiL2RwYy1hYy1vbmxpbmVyZmIucGRmMIIBBgYDVR0fBIH+MIH7MFWgU6BRhk9odHRwOi8vaWNwLWJyYXNpbC52YWxpZGNlcnRpZmljYWRvcmEuY29tLmJyL2FjLW9ubGluZXJmYi9sY3ItYWMtb25saW5lcmZidjIuY3JsMFagVKBShlBodHRwOi8vaWNwLWJyYXNpbDIudmFsaWRjZXJ0aWZpY2Fkb3JhLmNvbS5ici9hYy1vbmxpbmVyZmIvbGNyLWFjLW9ubGluZXJmYnYyLmNybDBKoEigRoZEaHR0cDovL3JlcG9zaXRvcmlvLmljcGJyYXNpbC5nb3YuYnIvbGNyL1ZBTElEL2xjci1hYy1vbmxpbmVyZmJ2Mi5jcmwwDgYDVR0PAQH/BAQDAgXgMB0GA1UdJQQWMBQGCCsGAQUFBwMCBggrBgEFBQcDBDCBuwYDVR0RBIGzMIGwgRhqZWZlcnNvbkBkanN5c3RlbS5jb20uYnKgOAYFYEwBAwSgLwQtMTgwODE5NzExMjI5ODczODgwNzAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwoCYGBWBMAQMCoB0EG0pFRkVSU09OIEdBQlJJRUwgREUgQUxNRUlEQaAZBgVgTAEDA6AQBA4wNTQ4MTMzNjAwMDEzN6AXBgVgTAEDB6AOBAwwMDAwMDAwMDAwMDAwDQYJKoZIhvcNAQELBQADggIBAHEyyqUFh3Jd+1fwqOyTT6CnSDnmUtYmNyFvDtKIu0jCyBOUuXiFyMTlqwyguJj9FBYhH0jyjbSUB3HotXdSEsHxQfIlMbWc1saG06rrEnZOKKpwHQUlz9ZAsuQlcC1tYXrA5uzRrsDLedQE3Z4oip/ExpECflIfODcrD5aHx/EyhM5NtTx+5EfN6N+7nQJ/6YbxGaEK2yExD7nitIKc/39HiOpsSu5qkjRXFBxt9tU6Vye3WQ6za74GaBz/eRL/adVAcyxjC6DC5mUu9nSonw8SCBcDKn7xOlu7pSwozZ1PUaFBbY5FxVKMfWDSCasG89o2PjquiMI52PSCpdncRj0piWOSSncmaWIWnzMAjtw6kCpylb1RRKxrFFS2y0/nEPkBkTnqUtKj07TJeUbnPyIHtYzrLvw5fgdDNMn2leENgouJiOEqmNS1vmFHsdC10Df1SmeNRjRFmDHLQARlIIna62F0RY9l/vqtrDUzyhiLPrefwOWd76ZaXkgv9w5JkyS+dalnzYyAlb2XQjFidup6970hEwNDmKggynaMmcnS4emQAklTRYpn1+6OJnmiRUE2wEY59bSAHqqHEZiBeXzgyqUg+ERE9EX6XmqtAoSbjqbmQWeWyIq1Rp/XRWB04lgDRp0/X4nfoX9fXVe/P0MLPrIgHfWMCp+m36eQP7Pg</X509Certificate></X509Data></KeyInfo></Signature></NFe><protNFe versao=\"4.00\"><infProt><tpAmb>2</tpAmb><verAplic>SVRSnfce201806291346</verAplic><chNFe>33180705481336000137651000000916401000005909</chNFe><dhRecbto>2018-07-03T15:03:06-03:00</dhRecbto><nProt>333180005216730</nProt><digVal>7kCR1M/dm/DQM0WmQyXSLj7r6P4=</digVal><cStat>100</cStat><xMotivo>Autorizado o uso da NF-e</xMotivo></infProt></protNFe></nfeProc>"

TODAS_IMPRESSORAS = ["i7", "i8", "i9", "ix", "Fitpos", "BK-T681", "MP-4200", "MP-2800", "DR800", "TM_T20", "TM-T20X",
                     "Print ID Touch", "TP-650", "PRINT DUAL"]


def toByte(texto: str):
    return bytes(texto, 'utf-8')


def base64ToString(b64: str):
    sample_string_bytes = base64.b64decode(b64)
    return toByte(sample_string_bytes.decode("ascii").encode().decode('unicode_escape'))


class E1:
    def __init__(self):
        if 'nt' in os.name:
            self.dll = DLL("C:\\users\\vagrant\\connect_device\\DLLS\\E1.dll")
        else:
            self.dll = DLL("/home/vagrant/DLLS/libE1_Impressora.so.01.04.06")

    @command_block('AbreConexao', '1.0', TODAS_IMPRESSORAS, category="conexao",
                   options=[
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["USB", "1"],
                               ["RS232", "2"],
                               ["TCP / IP", "3"],
                               ["Bluetooth", "4"],
                               ["Impressoras acopladas", "5"],
                           ],
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["i7", "i7"],
                               ["i8", "i8"],
                               ["i9", "i9"],
                               ["ix", "ix"],
                               ["Fitpos", "Fitpos"],
                               ["BK - T681", "BK - T681"],
                               ["MP - 4200", "MP - 4200"],
                               ["MP - 2800", "MP - 2800"],
                               ["DR800", "DR800"],
                               ["TM_T20", "TM_T20"],
                               ["TM - T20X", "TM - T20X"],
                               ["Print ID Touch", "Print ID Touch"],
                           ]
                       },
                       {
                           "type": "str",
                           "defaultvalue": "Endereço Impressora",
                       },
                       {
                           "type": "int",
                           "defaultvalue": 9100
                       }
                   ],
                   title="Conexão",
                   message="Abre conexão com a impressora. <br/> <b>Tipo:</b> Parâmetro do tipo numérico que define o tipo de comunicação que será estabelecido.<br/> <b>Modelo:</b> Parâmetro tipo caractere que especifica o modelo para conexão.<br/> <b>Conexao:</b> Parâmetro tipo caractere que define detalhes de conexão. <br/> <b>Parametro:</b> Parâmetro do tipo numérico auxiliar para a conexão com a impressora.",
                   )
    def AbreConexaoImpressora(self, Tipo: int, Modelo: str, Conexao: str, Parametro: int):
        func = self.dll.DLL_instance.AbreConexaoImpressora
        func.restype = c_int
        func.argtypes = [c_int, c_char_p, c_char_p, c_int]
        return func(Tipo, toByte(Modelo), toByte(Conexao), Parametro)

    @command_block('AbreConexao.2', '1.0', TODAS_IMPRESSORAS, category="conexao",
                   options=[
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["USB", "1"],
                               ["RS232", "2"],
                               ["TCP / IP", "3"],
                               ["Bluetooth", "4"],
                               ["Impressoras acopladas", "5"],
                           ],
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["i7", "i7"],
                               ["i8", "i8"],
                               ["i9", "i9"],
                               ["ix", "ix"],
                               ["Fitpos", "Fitpos"],
                               ["BK-T681", "BK-T681"],
                               ["MP-4200", "MP-4200"],
                               ["MP-2800", "MP-2800"],
                               ["DR800", "DR800"],
                               ["TM_T20", "TM_T20"],
                               ["TM-T20X", "TM-T20X"],
                               ["Print ID Touch", "Print ID Touch"],
                           ]
                       },
                   ],
                   title="Conexão",
                   message="Abre conexão com a impressora. <br/> <b>Tipo:</b> Parâmetro do tipo numérico que define o tipo de comunicação que será estabelecido.<br/> <b>Modelo:</b> Parâmetro tipo caractere que especifica o modelo para conexão.<br/> <b>Conexao:</b> Parâmetro tipo caractere que define detalhes de conexão. <br/> <b>Parametro:</b> Parâmetro do tipo numérico auxiliar para a conexão com a impressora.",
                   )
    def AbreConexaoImpressoraModo2(self, Tipo: int, Modelo, Conexao: str, Parametro: int):
        func = self.dll.DLL_instance.AbreConexaoImpressora
        func.restype = c_int
        func.argtypes = [c_int, c_char_p, c_char_p, c_int]
        return func(Tipo, toByte(Modelo), toByte(Conexao), Parametro)

    @command_block('ImpressaoTexto', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "str",
                           "defaultvalue": "Elgin Smart Test"
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["Esquerda", "0"],
                               ["Centro", "1"],
                               ["Direita", "2"],
                           ]
                       },
                       {
                           "type": "int",
                           "defaultvalue": 0
                       },
                       {
                           "type": "int",
                           "defaultvalue": 8
                       },
                   ],
                   title="Tipos de Impressão",
                   message='''Envia informações de texto para o buffer da impressora. As informações são impressas quando o buffer atinge o limite, quando é executada função AvancaPapel ou quando recebe um byte 10 (Line Feed – LF).Caso nenhuma dessas opções seja executada um próximo comando pode apagar os dados do buffer.<br/>
                   <b>Dados:</b> Parâmetro tipo caractere com a informação que será enviada ao buffer de impressão.<br/>
                   <b>Posicao:</b> Parâmetro tipo numérico que define a posição do texto a ser impresso.<br/>
                   <b>Estilo:</b> Parâmetro numérico que altera o estilo do texto impresso.<br/>
                   <bTamanho:</b> Define o tamanho do texto a ser impresso.'''
                   )
    def ImpressaoTexto(self, Dados: str, Posicao: int, Estilo: int, Tamanho: int):
        func = self.dll.DLL_instance.ImpressaoTexto
        func.restype = c_int
        func.argtypes = [c_char_p, c_int, c_int, c_int]
        return func(toByte(Dados), Posicao, Estilo, Tamanho)

    @command_block('ImpressaoTexto.2', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "str",
                           "defaultvalue": "Elgin Smart Test"
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["Esquerda", "0"],
                               ["Centro", "1"],
                               ["Direita", "2"],
                           ]
                       },
                       {
                           "type": "checkboxs",
                           "defaultvalues": [
                               "0 - Fonte A",
                               "1 - Fonte B",
                               "2 - Sublinhado",
                               "4 - Modo Reverso",
                               "8 - Negrito",
                           ]
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["1 x na altura e largura", "0"],
                               ["2 x na altura", "1"],
                               ["3 x na altura", "2"],
                               ["4 x na altura", "3"],
                               ["5 x na altura", "4"],
                               ["6 x na altura", "5"],
                               ["7 x na altura", "6"],
                               ["8 x na altura", "7"],
                           ]
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["1 x na altura / largura", "0"],
                               ["2 x na largura", "16"],
                               ["3 x na largura", "32"],
                               ["4 x na largura", "48"],
                               ["5 x na largura", "64"],
                               ["6 x na largura", "80"],
                               ["7 x na largura", "96"],
                               ["8 x na largura", "112"]
                           ]
                       },
                   ],
                   title="Tipos de Impressão",
                   message='''Envia informações de texto para o buffer da impressora.<br/> As informações são impressas quando o buffer atinge o limite, quando é executada a função AvancaPapel ou quando recebe um byte 10 (Line Feed – LF). Caso nenhuma dessas opções seja executada um próximo comando pode apagar os dados do buffer.<br/>
                   <b>Dados:</b> Parâmetro tipo caractere com a informação que será enviada ao buffer de impressão.<br/>
                   <b>Posicao:</b> Parâmetro tipo numérico que define a posição do texto a ser impresso.<br/>
                   <b>Estilo:</b> Parâmetro numérico que altera o estilo do texto impresso.<br/>
                   <b>Tamanho:</b> Define o tamanho do texto a ser impresso.''',
                   )
    def ImpressaoTextoModo2(self,
                            Dados: str,
                            Posicao: int,
                            stilo: int,
                            Altura: int,
                            Largura: int,
                            Check4: int,
                            Check5: int,
                            TamanhoAltura: int,
                            TamanhoLargura: int):
        stilo = stilo + Altura + Largura + Check4 + Check5
        func = self.dll.DLL_instance.ImpressaoTexto
        func.restype = c_int
        func.argtypes = [c_char_p, c_int, c_int, c_int]
        return func(toByte(Dados), Posicao, stilo, TamanhoAltura + TamanhoLargura)

    @command_block('ImpressaoQRCode', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "str",
                           "defaultvalue": "Elgin Smart Test"
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 1,
                               "max": 6
                           },
                           "defaultvalue": 3
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["75%", "1"],
                               ["15%", "2"],
                               ["25%", "3"],
                               ["30%", "4"],
                           ]
                       },
                   ],
                   title="Tipos de Impressão",
                   message='''Impressão de QRCode. Essa função imprime o código QRCode com possibilidade de variação de tamanho e nível de correção.<br/>
                   <b>Dados:</b> Conjunto de informações que irão compor o QRCode.<br/>
                   <b>Tamanho:</b> Tamanho do QRCode.<br/>
                   <b>NivelCorrecao:</b> Define o nível de correção a ser configurado para o QRCode.''',
                   )
    def ImpressaoQRCode(self, Dados: str, Tamanho: int, NivelCorrecao: int):
        func = self.dll.DLL_instance.ImpressaoQRCode
        func.restype = c_int
        func.argtypes = [c_char_p, c_int, c_int]
        return func(toByte(Dados), Tamanho, NivelCorrecao)

    @command_block('FechaConexao', '1.0', TODAS_IMPRESSORAS,  category="conexao", title="Conexão", message="Fecha conexão com a impressora. Finaliza a conexão que foi estabelecida em AbreConexaoImpressora."
                   )
    def FechaConexao(self):
        func = self.dll.DLL_instance.FechaConexaoImpressora
        func.restype = c_int
        func.argtypes = []
        return func()

    @command_block('Corte', '1.0', TODAS_IMPRESSORAS,   category='config_Impressao',
                   options=[
                       {
                           "type": "int",
                           "defaultvalue": 0
                       },
                   ],
                   title="Configuração de Impressão",
                   message="Realiza o corte do papel.<br/> <b>OBS:</b> A impressora do SmartPOS não suporta este método, uma vez que não existe guilhotina nesse modelo. Este método funciona corretamente nas demais impressoras térmicas, conectadas ao SmartPOS.<br/> <b>Avanco:</b> Parâmetro numérico que indica o quanto o papel deve avançar antes do corte."
                   )
    def Corte(self, Avanco: int):
        func = self.dll.DLL_instance.Corte
        func.restype = c_int
        func.argtypes = [c_int]
        return func(Avanco)

    @command_block('GetVersaoDLL', '1.0', TODAS_IMPRESSORAS, category="suporte", title="Suporte", message='Retorna a versão da DLL')
    def GetVersaoDLL(self):
        func = self.dll.DLL_instance.GetVersaoDLL
        func.restype = c_char_p
        func.argtypes = []
        return func()

    @command_block('EspacamentoEntreLinhas', '1.0', TODAS_IMPRESSORAS,  category='config_Impressao',
                   options=[
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 1,
                               "max": 255
                           },
                           "defaultvalue": 50
                       },
                   ],
                   title="Configuração de Impressora",
                   message="Altera o espaçamento entre linhas na impressão.<br/> <b>Tamnaho:</b> Define espaçamento. Valor entre 1 e 255."
                   )
    def EspacamentoEntreLinhas(self, Tamanho: int):
        func = self.dll.DLL_instance.EspacamentoEntreLinhas
        func.restype = c_int
        func.argtypes = [c_int]
        return func(Tamanho)

    @command_block('CorteTotal', '1.0', TODAS_IMPRESSORAS, category='config_Impressao',
                   options=[
                       {
                           "type": "int",
                           "defaultvalue": 0
                       },
                   ],
                   title='Configuração de Impressão',
                   message="Realiza o corte total do papel.<br/> <b>OBS:</b> Algumas impressoras não suporta este método devido a limitação do hardware. As impressoras com tal limitação irão executar o corte parcial.<br/> <b>Avanco:</b> Parâmetro numérico que indica o quanto o papel deve avançar antes do corte."
                   )
    def CorteTotal(self, Avanco: int):
        func = self.dll.DLL_instance.CorteTotal
        func.restype = c_int
        func.argtypes = [c_int]
        return func(Avanco)

    @command_block('ImpressaoPDF417', '1.0', TODAS_IMPRESSORAS,  category="tipos_Impressao",
                   options=[
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 0,
                               "max": 30
                           },
                           "defaultvalue": 15
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 3,
                               "max": 90
                           },
                           "defaultvalue": 50
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 2,
                               "max": 8
                           },
                           "defaultvalue": 5
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 2,
                               "max": 8
                           },
                           "defaultvalue": 5
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["Nível 0", "0"],
                               ["Nível 1", "1"],
                               ["Nível 2", "2"],
                               ["Nível 3", "3"],
                               ["Nível 4", "4"],
                               ["Nível 5", "5"],
                               ["Nível 6", "6"],
                               ["Nível 7", "7"],
                               ["Nível 8", "8"],
                           ]
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["Standard", "0"],
                               ["Truncated", "1"],
                           ]
                       },
                       {
                           "type": "str",
                           "defaultvalue": "Elgin Smart Test"
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Impressão de código PDF417.<br/> Realiza a impressão de PDF417, com possibilidade de variação de tamanho e nível de correção.<br/> <b>QuantidadeColuna:</b> Parâmetro do tipo numérico que define o número de colunas.<br/> <b>QuantidadeLinha:</b> Parâmetro do tipo numérico que define o número de linhas.<br/> <b>Largura:</b> Parâmetro do tipo numérico que define a largura do módulo, em pontos.<br/> <b>Altura:</b> Parâmetro do tipo numérico que define a altura da linha, multiplicando o valor definido pela largura do módulo (configurada em width).<br/> <b>NivelCorrecao:</b> Parâmetro do tipo numérico que define o nível de correção de erro.<br/> <b>Opcoes:</b> Parâmetro do tipo numérico que define a opção para PDF417. <br/> <b>Dados:</b> Parâmetro do tipo caractere que define o código a ser impresso."
                   )
    def ImpressaoPDF417(self, QuantidadeColuna: int, QuantidadeLinha: int, Largura: int, Altura: int, NivelCorrecao: int, Opcoes: int,
                        Dados: str):
        func = self.dll.DLL_instance.ImpressaoPDF417
        func.restype = c_int
        func.argtypes = [c_int, c_int, c_int, c_int, c_int, c_int, c_char_p]
        return func(QuantidadeColuna, QuantidadeLinha, Largura, Altura, NivelCorrecao, Opcoes, toByte(Dados))

    @command_block('ImpressaoCodigoBarras', '1.0', TODAS_IMPRESSORAS,  category="tipos_Impressao",
                   options=[
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["UPC-A", "0"],
                               ["UPC-E", "1"],
                               ["JAN13 / EAN 13", "2"],
                               ["JAN8 / EAN 8", "3"],
                               ["CODE 39", "4"],
                               ["TF", "5"],
                               ["CODE BAR", "6"],
                               ["CODE 93", "7"],
                               ["CODE 128", "8"],
                           ]
                       },
                       {
                           "type": "str",
                           "defaultvalue": "Elgin Smart Test"
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 1,
                               "max": 255
                           },
                           "defaultvalue": 50
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 1,
                               "max": 6
                           },
                           "defaultvalue": 3
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["Acima do código", "1"],
                               ["Abaixo do código", "2"],
                               ["Ambos", "3"],
                               ["Não impresso", "4"],
                           ]
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Impressão de código de barras.<br/> Realiza a impressão de código de barras.<br/> <b>Tipo:</b> Define o modelo do código de barras a ser impresso.<br/> <b>Dados:</b> Informação que compõe o código.<br/> <b>Altura:</b> Altura do código de barras.<br/> <b>Largura:</b> Define a largura do código de barras mas se valor definido ultrapassar área de impressão o código não será impresso.<br/> <b>Posicao:</b/> Define a posição de impressão do conteúdo do código de barras."
                   )
    def ImpressaoCodigoBarras(self, Tipo: int, Dados: str, Altura: int, Largura: int, Posicao: int):
        func = self.dll.DLL_instance.ImpressaoCodigoBarras
        func.restype = c_int
        func.argtypes = [c_int, c_char_p, c_int, c_int, c_int]
        return func(Tipo, toByte(Dados), Altura, Largura, Posicao)

    @command_block('AvancaPapel', '1.0', TODAS_IMPRESSORAS, category='config_Impressao',
                   options=[
                       {
                           "type": "int",
                           "defaultvalue": 0
                       },
                   ],
                   title="Configuração de Impressão",
                   message="Imprime informações no buffer e avança o papel.<br/> <b>Linhas:</b> Indica o quanto o papel deve avançar."
                   )
    def AvancaPapel(self, Linhas: int):
        func = self.dll.DLL_instance.AvancaPapel
        func.restype = c_int
        func.argtypes = [c_int]
        return func(Linhas)

    @command_block('StatusImpressora', '1.0', TODAS_IMPRESSORAS, category="suporte",
                   options=[
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["Gaveta", "1"],
                               ["Tampa", "2"],
                               ["Papel", "3"],
                               ["Ejetor", "4"],
                               ["Geral", "5"],
                           ]
                       },
                   ],
                   title="Suporte",
                   message="Obtém o status da impressora.<br/> Essa função disponibiliza o status de gaveta, tampa, sensor de papel e do ejetor.<br/> <b>Parametro:</b> Parâmetro tipo numérico para indicar qual status deve ser retornado."
                   )
    def StatusImpressora(self, Parametro: int):
        func = self.dll.DLL_instance.StatusImpressora
        func.restype = c_int
        func.argtypes = [c_int]
        return func(Parametro)

    @command_block('AbreGavetaElgin', '1.0', TODAS_IMPRESSORAS, category="suporte", title="Suporte", message="Abre gavetas Elgin.<br/> Essa função usa parâmetros padrões para abertura de gavetas Elgin. Para abrir gavetas de marcas diferentes use AbreGaveta e envie os parâmetros necessários para abertura."
                   )
    def AbreGavetaElgin(self):
        func = self.dll.DLL_instance.AbreGavetaElgin
        func.restype = c_int
        func.argtypes = []
        return func()

    @command_block('AbreGaveta', '1.0', TODAS_IMPRESSORAS, category="suporte",
                   options=[
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["0", "0"],
                               ["1", "1"],
                           ]
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 1,
                               "max": 255
                           },
                           "defaultvalue": 50
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 1,
                               "max": 255
                           },
                           "defaultvalue": 50
                       },
                   ],
                   title="Suporte",
                   message="Abre gaveta. Essa função abre gavetas de acordo com os parâmetros fornecidos.<br/> <b>Pino:</b> Indicação do pino a ser acionado.<br/> <b>TempoInicialização:</b> Tempo de inicialização do pulso.<br/> <b>TempoDesativacao:</b> Tempo de desativação do pulso."
                   )
    def AbreGaveta(self, Pino: int, TempoInicializacao: int, TempoDesativacao: int):
        func = self.dll.DLL_instance.AbreGaveta
        func.restype = c_int
        func.argtypes = [c_int, c_int, c_int]
        return func(Pino, TempoInicializacao, TempoDesativacao)

    @command_block('InicializaImpressora', '1.0', TODAS_IMPRESSORAS, category="conexao",
                   title="Conexão",
                   message=" Inicializa a impressora para novas tarefas. Essa função inicializa a impressora para novas tarefas, redefine as configurações para o padrão da impressora e apaga as informações em buffer."
                   )
    def InicializaImpressora(self):
        func = self.dll.DLL_instance.InicializaImpressora
        func.restype = c_int
        func.argtypes = []
        return func()

    @command_block('DefinePosicao', '1.0', TODAS_IMPRESSORAS, category="config_Impressao",
                   options=[
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["Esquerda", "0"],
                               ["Centro", "1"],
                               ["Direita", "2"],
                           ]
                       },
                   ],
                   title="Configuração de Impressão",
                   message="Define a posição do conteúdo a ser impresso.<br/> <b> OBS:</b> As impressoras do SmartPOS e do Mini PDV M8 suportam parcialmente este método; nas demais impressoras térmicas, conectadas nesses dispositivos, este método funciona completamente.<br/> <b>Posicao:</b> Parâmetro tipo numérico para indicar a posição."
                   )
    def DefinePosicao(self, Posicao: int):
        func = self.dll.DLL_instance.DefinePosicao
        func.restype = c_int
        func.argtypes = [c_int]
        return func(Posicao)

    @command_block('SinalSonoro', '1.0', TODAS_IMPRESSORAS,  category="suporte",
                   options=[
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 1,
                               "max": 63
                           },
                           "defaultvalue": 50
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 1,
                               "max": 25
                           },
                           "defaultvalue": 15
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 1,
                               "max": 25
                           },
                           "defaultvalue": 15
                       },
                   ],
                   title="Suporte",
                   message="Emite sinal sonoro.<br/> <b>OBS:</b> O SmartPOS e o Mini PDV M8 não suportam este método; contudo, este método funciona corretamente nas demais impressoras térmicas, conectadas nesses dispositivos. Emite sinal sonoro na impressora. Algumas impressoras não estão habilitadas para emitir sinal sonoro.<br/> <b>Quantidade:</b> Define quantidade de sinais emitidos.<br/> <b>TempoInicio:</b> Define o tempo em que o sinal deve ficar ativo.<br/> <b>TempoFim:</b> Define o tempo entre um sinal e outro."
                   )
    def SinalSonoro(self, Quantidade: int, TempoInicio: int, TempoFim: int):
        func = self.dll.DLL_instance.SinalSonoro
        func.restype = c_int
        func.argtypes = [c_int, c_int, c_int]
        return func(Quantidade, TempoInicio, TempoFim)

    @command_block('DirectIO', '1.0', TODAS_IMPRESSORAS,  category="suporte",
                   options=[
                       {
                           "type": "str",
                           "defaultvalue": ""
                       },
                       {
                           "type": "int",
                           "defaultvalue": 5
                       },
                       {
                           "type": "str",
                           "defaultvalue": ""
                       },
                       {
                           "type": "int",
                           "defaultvalue": 0
                       },
                   ],
                   title="Suporte",
                   message="Envia comandos ESCPOS direto para a porta de comunicação.<br/> <b>OBS:</b> Este método não está disponível para uso com o g8. OBS: Este método apresenta algumas diferenças no ambiente Android - consulte sua documentação em Módulos > Android > Módulos > Impressora Térmica > DirectIO. Função de despejo de comandos na porta de comunicação. Essa função também recebe dados da porta de comunicação.<br/> <b>Comandos:</b> Sequência de comandos que devem ser enviados para a porta de comunicação.<br/> <b>QuantidadeComandos:</b> Quantidade de comandos enviados e que serão escritos.<br/> <b>AlocacaoMemoria:</b> Alocação de memória que receberá os dados de retorno caso seja necessário.<br/> <b>QuantidadeLeituraDados:</b> Quantidade de dados que se espera ler. Após a execução da função, essa variável deve indicar a quantidade de dados lidos da porta. Para evitar a leitura da porta esse parâmetro deve estar em ZERO (0)."
                   )
    def DirectIO(self, Comandos: str, QuantidadeComandos: int, AlocacaoMemoria: str, QuantidadeLeituraDados: int):
        func = self.dll.DLL_instance.DirectIO
        func.restype = c_int
        func.argtypes = [c_char_p, c_int, c_char_p, c_int]
        return func(toByte(Comandos), QuantidadeComandos, toByte(AlocacaoMemoria), QuantidadeLeituraDados)

    @command_block('ImprimeImagemMemoria', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "str",
                           "defaultvalue": "100"
                       },
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["Normal", "1"],
                               ["Duplicado", "2"],
                           ]
                       },
                   ],
                   title="Tipos de mpressão",
                   message="Imprime informações no buffer e avança o papel.<br/> <b>Chave:</b>  Par de chaves identificador da imagem.<br/> <b>Escala:</b> Tamanho da impressão, podendo ser 1 ou 2 para normal e duplicado, respectivamente."
                   )
    def ImprimeImagemMemoria(self, Chave: str, Escala: int):
        func = self.dll.DLL_instance.ImprimeImagemMemoria
        func.restype = c_int
        func.argtypes = [c_char_p, c_int]
        return func(toByte(Chave), Escala)

    @command_block('ImprimeXMLSAT', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "file",
                           "defaultvalue": "URL XML",
                       },
                       {
                           "type": "int",
                           "defaultvalue": 0
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Imprime Danfe SAT. Essa função recebe o XML de retorno da venda do SAT, valida o conteúdo, constrói o Danfe e realiza a impressão de acordo com a especificação da SEFAZ SP. <br/> <b>Dados:</b> Conteúdo do XML de retorno da venda do SAT ou Caminho para arquivo com dados do SAT prefixado com path=.<br/> <b>Parametro:</b> Parâmetro do tipo numérico para ativar bits que modificam o cupom a ser impresso."
                   )
    def ImprimeXMLSAT(self, Dados: str, Parametro: int):
        func = self.dll.DLL_instance.ImprimeXMLSAT
        func.restype = c_int
        func.argtypes = [c_char_p, c_int]
        Dados = base64ToString(Dados)
        return func(Dados, Parametro)

    @command_block('ImprimeXMLSAT.2', '1.0', TODAS_IMPRESSORAS,  category="tipos_Impressao",
                   options=[
                       {
                           "type": "file",
                           "defaultvalue": "URL XML",
                       },
                       {
                           "type": "checkboxs",
                           "defaultvalues": [
                               "1 - Impressão de logo no cabeçalho",
                               "2 - Extrato reduzido",
                               "4 - Cupom em ambiente de teste",
                               "64 - Registro de item com desconto ou acréscimo e variações no grupo totais",
                               "128 - Imprime usando novo layout",
                               "256 - Ativa separadores no novo layout",
                           ]
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Imprime Danfe SAT. Essa função recebe o XML de retorno da venda do SAT, valida o conteúdo, constrói o Danfe e realiza a impressão de acordo com a especificação da SEFAZ SP. <br/> <b>Dados:</b> Conteúdo do XML de retorno da venda do SAT ou Caminho para arquivo com dados do SAT prefixado com path=.<br/> <b>Parametro:</b> Parâmetro do tipo numérico para ativar bits que modificam o cupom a ser impresso."
                   )
    def ImprimeXMLSATModo2(self, Dados: str, Parametro: int, check2: int, check3: int,
                           check4: int, check5: int, check6: int):

        param_sum = Parametro + check2 + check3 + check4 + check5 + check6

        func = self.dll.DLL_instance.ImprimeXMLSAT
        func.restype = c_int
        func.argtypes = [c_char_p, c_int]
        Dados = base64ToString(Dados)
        return func(Dados, param_sum)

    @command_block('ImprimeXMLCancelamentoSAT', '1.0', TODAS_IMPRESSORAS,  category="tipos_Impressao",
                   options=[
                       {
                           "type": "file",
                           "defaultvalue": "URL XML",
                       },
                       {
                           "type": "str",
                           "defaultvalue": "Teste2",
                       },
                       {
                           "type": "int",
                           "defaultvalue": 0
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Imprime Danfe de cancelamento SAT. Essa função recebe o XML de retorno da operação de cancelamento e os dados de assinatura do QRCode de venda, valida as informações, constrói o Danfe e realiza impressão de acordo com a especificação da SEFAZ SP.<br/> <b>Dados:</b> Conteúdo do XML de cancelamento retornado pelo SAT ou Caminho para arquivo com dados do SAT prefixado com path=.<br/> <b>AssinaturaQRCode:</b> Assinatura do QRCode retornado na operação de Venda. Essa informação é necessária porque o XML de retorno da operação de venda não a contém.<br/> <b>Parametro:</b> Parâmetro do tipo numérico para ativar bits que modificam o cupom a ser impresso."
                   )
    def ImprimeXMLCancelamentoSAT(self, Dados: str, AssinaturaQRCode: str, Parametro: int):
        func = self.dll.DLL_instance.ImprimeXMLCancelamentoSAT
        func.restype = c_int
        func.argtypes = [c_char_p, c_char_p, c_int]
        Dados = base64ToString(Dados)
        return func(Dados, toByte(AssinaturaQRCode), Parametro)

    @command_block('ImprimeXMLCancelamentoSAT2', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "file",
                           "defaultvalue": "Teste",
                       },
                       {
                           "type": "str",
                           "defaultvalue": "Assinatura",
                       },
                       {
                           "type": "checkboxs",
                           "defaultvalues": [
                               "1 - Imprime logo no cabeçalho",
                               "64 - Imprime usando novo layout",
                               "128 - Ativa separadores no novo layout",
                           ]
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Imprime Danfe de cancelamento SAT. Essa função recebe o XML de retorno da operação de cancelamento e os dados de assinatura do QRCode de venda, valida as informações, constrói o Danfe e realiza impressão de acordo com a especificação da SEFAZ SP.<br/> <b>Dados:</b> Conteúdo do XML de cancelamento retornado pelo SAT ou Caminho para arquivo com dados do SAT prefixado com path=.<br/> <b>AssinaturaQRCode:</b> Assinatura do QRCode retornado na operação de Venda. Essa informação é necessária porque o XML de retorno da operação de venda não a contém.<br/> <b>Parametro:</b> Parâmetro do tipo numérico para ativar bits que modificam o cupom a ser impresso."
                   )
    def ImprimeXMLCancelamentoSAT2(self, Dados: str, assQRCode: str,
                                   stilo: int, check2: int, check3: int
                                   ):
        parametro = stilo + check2 + check3
        func = self.dll.DLL_instance.ImprimeXMLCancelamentoSAT
        func.restype = c_int
        func.argtypes = [c_char_p, c_char_p, c_int]
        Dados = base64ToString(Dados)
        return func(Dados, toByte(assQRCode), parametro)

    @command_block('ImprimeXMLCancelamentoNFCe', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "file",
                           "defaultvalue": "Teste",
                       },
                       {
                           "type": "int",
                           "defaultvalue": 0,
                       },
                   ],
                   title="Tipos de Impressão",
                   message="ImprimeXMLCancelamentoNFCe Essa função recebe o conteúdo do XML de cancelamento do NFCe, valida o conteúdo, constrói o Danfe e realiza a impressão.<br/> <b>Dados:</b> Conteúdo do XML de cancelamento retornado pelo SAT ou Caminho para arquivo com dados do SAT prefixado com path=.<br/> <b>Parametro:</b> Reservado para customizações. Passar valor ZERO."
                   )
    def ImprimeXMLCancelamentoNFCe(self, Dados: str, parametro: int):
        func = self.dll.DLL_instance.ImprimeXMLCancelamentoNFCe
        func.restype = c_int
        func.argtypes = [c_char_p, c_int]
        Dados = base64ToString(Dados)
        return func(Dados, parametro)

    @command_block('ImprimeXMLNFCe', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "file",
                           "defaultvalue": "URL XML",
                       },
                       {
                           "type": "int",
                           "defaultvalue": 1,
                       },
                       {
                           "type": "str",
                           "defaultvalue": "CODIGO-CSC-CONTRIBUINTE",
                       },
                       {
                           "type": "int",
                           "defaultvalue": 0
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Imprime o Danfe NFCe. Essa função recebe o conteúdo do XML de venda do NFCe, valida o conteúdo, constrói o Danfe e realiza a impressão.<br/> <b>Dados:</b> Conteúdo do XML retornado pela venda ou Caminho para arquivo com dados da NFCe prefixado com path=.<br/> <b>IndexCSC:</b> Identificador do CSC (Código de Segurança do Contribuinte no Banco de Dados da SEFAZ). Deve ser informado sem os “0” (zeros) não significativos. A identificação do CSC corresponde à ordem do CSC no banco de dados da SEFAZ, não confundir com o próprio CSC.<br/> <b>CSC:</b> Código de Segurança do Contribuinte.<br/> <b>Parametro:</b> Parâmetro do tipo numérico para ativar bits que modificam o cupom a ser impresso.",
                   )
    def ImprimeXMLNFCe(self, Dados: str, IndexCSC: int, CSC: str, Parametro: int):
        func = self.dll.DLL_instance.ImprimeXMLNFCe
        func.restype = c_int
        func.argtypes = [c_char_p, c_int, c_char_p, c_int]
        Dados = base64ToString(Dados)
        return func(Dados, IndexCSC, toByte(CSC), Parametro)

    @command_block('ImprimeXMLNFCe.2', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "file",
                           "defaultvalue": "Teste",
                       },
                       {
                           "type": "int",
                           "defaultvalue": 1,
                       },
                       {
                           "type": "str",
                           "defaultvalue": "CODIGO-CSC-CONTRIBUINTE",
                       },
                       {
                           "type": "checkboxs",
                           "defaultvalues": [
                               "1 - Extrato resumido",
                               "2 - Utilizar o EAN 13 no código dos produtos",
                               "4 - Completa de descrição",
                               "8 - Logo da empresa em memória",
                               "16 - Cupom de homologação",
                               "32 - Redução do espaço de impressão",
                               "64 - Utilizar separadores de sessão para o cupom",
                               "128 - IE no cabeçalho",
                               "256 - Número sequencial",
                               "512 - Via para Danfe em contingência",
                               "1024 - Acréscimos/descontos por item",
                           ]
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Imprime o Danfe NFCe. Essa função recebe o conteúdo do XML de venda do NFCe, valida o conteúdo, constrói o Danfe e realiza a impressão.<br/> <b>Dados:</b> Conteúdo do XML retornado pela venda ou Caminho para arquivo com dados da NFCe prefixado com path=.<br/> <b>IndexCSC:</b> Identificador do CSC (Código de Segurança do Contribuinte no Banco de Dados da SEFAZ). Deve ser informado sem os “0” (zeros) não significativos. A identificação do CSC corresponde à ordem do CSC no banco de dados da SEFAZ, não confundir com o próprio CSC.<br/> <b>CSC:</b> Código de Segurança do Contribuinte.<br/> <b>Parametro:</b> Parâmetro do tipo numérico para ativar bits que modificam o cupom a ser impresso.",
                   )
    def ImprimeXMLNFCeModo2(self, Dados: str, IndexCSC: int, CSC: str,
                            parametro: int, check2: int, check3: int,
                            check4: int, check5: int, check6: int,
                            check7: int, check8: int, check9: int,
                            check10: int, check11: int
                            ):
        param_sum = parametro + check2 + check3 + check4 + check5 + \
            check6 + check7 + check8 + check9 + check10 + check11
        func = self.dll.DLL_instance.ImprimeXMLNFCeModo
        func.restype = c_int
        func.argtypes = [c_char_p, c_int, c_char_p, c_int]
        Dados = base64ToString(Dados)
        return func(Dados, IndexCSC, toByte(CSC), param_sum)

    @command_block('ModoPagina', '1.0', TODAS_IMPRESSORAS, category="suporte", title="Suporte", message="Habilita Modo Página. Deve ser utilizado para dar início aos trabalhos em modo página.")
    def ModoPagina(self):
        func = self.dll.DLL_instance.ModoPagina
        func.restype = c_int
        func.argtypes = []
        return func()

    @command_block('DirecaoImpressao', '1.0', TODAS_IMPRESSORAS,  category="config_Impressao",
                   options=[
                       {
                           "type": "dropdown",
                           "defaultvalues": [
                               ["Da esquerda para direita", "0"],
                               ["De baixo para cima", "1"],
                               ["Da direita para esquerda", "2"],
                               ["De cima para baixo", "3"],
                           ]
                       },
                   ],
                   title="Configuração de Impressão",
                   message="Define Direção de Impressão.<br/> <b>OBS:</b> A impressora do SmartPOS não suporta este método; contudo, este método funciona corretamente nas demais impressoras térmicas, conectadas ao SmartPOS. Função do modo página utilizada para definir a direção em que serão realizadas as impressões.<br/> <b>Direcao:</b> Valor entre 0 e 3. ( 0 - Da esquerda para direita partindo do ponto superior esquerdo, 1 - De baixo para cima partindo do ponto inferior esquerdo, 2 - Da direita para esquerda partindo do ponto inferior direito, 3 - De cima para baixo partindo do ponto superior direito.)"
                   )
    def DirecaoImpressao(self, Direcao: int):
        func = self.dll.DLL_instance.DirecaoImpressao
        func.restype = c_int
        func.argtypes = [c_int]
        return func(Direcao)

    @command_block('DefineAreaImpressao', '1.0', TODAS_IMPRESSORAS, category="config_Impressao",
                   options=[
                       {
                           "type": "int",
                           "defaultvalue": 0,
                       },
                       {
                           "type": "int",
                           "defaultvalue": 0,
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 0,
                               "max": 580
                           },
                           "defaultvalue": 0
                       },
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 0,
                               "max": 6640
                           },
                           "defaultvalue": 0
                       },
                   ],
                   title="Configuração de Impressão",
                   message="Define Área Impressão.<br/> <b>OBS:</b> A impressora do SmartPOS não suporta este método; contudo, este método funciona corretamente nas demais impressoras térmicas, conectadas ao SmartPOS. Função do modo página usada para definir o tamanho da área de impressão.<br/> <b>InicialHorizontal:</b> Define o ponto inicial horizontal. Valor em pontos entre 0 e a sua definição do parâmetro DimensaoHorizontal.<br/> <b>InicialVertical:</b> Define ponto inicial na direção vertical. Valor em pontos entre 0 e a sua definição do parâmetro DimensaoVertical.<br/> <b>DimensaoHorizontal:</b> Define a dimensão na direção horizontal, valor em pontos entre 0 e 580.<br/> <b>DimensaoVertical:</b> Define a dimensão da direção vertical, valor em pontos entre 0 e 6640."
                   )
    def DefineAreaImpressao(self, InicialHorizontal: int, InicialVertical: int, DimensaoHorizontal: int, DimensaoVertical: int):
        func = self.dll.DLL_instance.DefineAreaImpressao
        func.restype = c_int
        func.argtypes = [c_int, c_int, c_int, c_int]
        return func(InicialHorizontal, InicialVertical, DimensaoHorizontal, DimensaoVertical)

    @command_block('PosicaoImpressaoHorizontal', '1.0', TODAS_IMPRESSORAS, category="config_Impressao",
                   options=[
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 0,
                               "max": 580
                           },
                           "defaultvalue": 0
                       },
                   ],
                   title="Configuração de Impressão",
                   message="Define Posição de Impressão Horizontal. Função do modo página usada para definir a posição de impressão Horizontal.<br/> <b>PontoInicialHorizontal:</b> Define o ponto inicial horizontal. Valor em pontos entre 0 e 580."
                   )
    def PosicaoImpressaoHorizontal(self, PontoInicialHorizontal: int):
        func = self.dll.DLL_instance.PosicaoImpressaoHorizontal
        func.restype = c_int
        func.argtypes = [c_int]
        return func(PontoInicialHorizontal)

    @command_block('PosicaoImpressaoVertical', '1.0', TODAS_IMPRESSORAS, category="config_Impressao",
                   options=[
                       {
                           "type": "slider",
                           "sliderOption": {
                               "min": 0,
                               "max": 6640
                           },
                           "defaultvalue": 0
                       },
                   ],
                   title="Configuração de Impressão",
                   message="Define a Posição da Impressão Vertical.<br/> <b>OBS:</b> A impressora do SmartPOS não suporta este método; contudo, este método funciona corretamente nas demais impressoras térmicas, conectadas ao SmartPOS. Função do modo página usada para definir a posição de impressão Vertical.<br/> <b>PontoInicialHorizontal:</b> Define o ponto inicial Vertical, valor em pontos entre 0 e 6640."
                   )
    def PosicaoImpressaoVertical(self, PontoInicialVertical: int):
        func = self.dll.DLL_instance.PosicaoImpressaoVertical
        func.restype = c_int
        func.argtypes = [c_int]
        return func(PontoInicialVertical)

    @command_block('ImprimeModoPagina', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao", title="Tipos de Impressão", message="Imprime Modo Página. Função que imprime em Modo Página."
                   )
    def ImprimeModoPagina(self):
        func = self.dll.DLL_instance.ImprimeModoPagina
        func.restype = c_int
        func.argtypes = []
        return func()

    @command_block('LimpaBufferModoPagina', '1.0', TODAS_IMPRESSORAS, category="config_Impressao", title="Configuração de Impressão", message="Limpa Buffer em Modo Página.<br/> <b>OBS:</b> A impressora do SmartPOS não suporta este método; contudo, este método funciona corretamente nas demais impressoras térmicas, conectadas ao SmartPOS. Função do modo página usada para Limpar o Buffer."
                   )
    def LimpaBufferModoPagina(self):
        func = self.dll.DLL_instance.LimpaBufferModoPagina
        func.restype = c_int
        func.argtypes = []
        return func()

    @command_block('ImprimeMPeRetornaPadrao', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao", title="Tipos de Impressão", message="Imprime Modo Página e Retorna ao Modo Padrão.<br/> <b>OBS:</b> A impressora do SmartPOS não suporta este método; contudo, este método funciona corretamente nas demais impressoras térmicas, conectadas ao SmartPOS. Função do modo página usada para Imprimir.")
    def ImprimeMPeRetornaPadrao(self):
        func = self.dll.DLL_instance.ImprimeMPeRetornaPadrao
        func.restype = c_int
        func.argtypes = []
        return func()

    @command_block('ModoPadrao', '1.0', TODAS_IMPRESSORAS, category="suporte", title="Suporte", message="Retorna ao Modo Padrão.<br/> <b>OBS:</b> A impressora do SmartPOS não suporta este método; contudo, este método funciona corretamente nas demais impressoras térmicas, conectadas ao SmartPOS. Função do modo página usada para Retornar ao Modo Padrão.")
    def ModoPadrao(self):
        func = self.dll.DLL_instance.ModoPadrao
        func.restype = c_int
        func.argtypes = []
        return func()

    @command_block('ImprimeCupomTEF', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "file",
                           "defaultvalue": "URL Cumpom TEF",
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Realiza a impressão do Cupom TEF. Essa função pode ser usada para imprimir o comprovante presente nos arquivos processados nos TEF que utilizem modelo de troca de arquivos. A função busca pelos registros 029-XXX.<br/> <b>Dados:</b> Conteúdo a ser impresso ou Caminho para arquivo com dados da venda prefixado com path="
                   )
    def ImprimeCupomTEF(self, Dados: str):
        func = self.dll.DLL_instance.ImprimeCupomTEF
        func.restype = c_int
        func.argtypes = [c_char_p]
        Dados = base64ToString(Dados)
        return func(Dados)

    @command_block('ImprimeImagem', '1.0', TODAS_IMPRESSORAS, category="tipos_Impressao",
                   options=[
                       {
                           "type": "file",
                           "defaultvalue": "",
                       },
                   ],
                   title="Tipos de Impressão",
                   message="Imprime imagem enviada à impressora diretamente, sem carregá-la na memória.<br/> <b>Path:</b> Parâmetro do tipo caractere que define a imagem que será impressa. Este parâmetro recebe como argumento o caminho, no sistema de arquivos, onde a imagem está armazenada, incluindo seu nome."
                   )
    def ImprimeImagem(self, b_64_image: str):
        [os_architecture, os_name] = platform.architecture()
        path_image = ""
        current_time = datetime.datetime.now()
        if 'Windows' in os_name:
            with open(f"C:/users/vagrant/connect_device/assets/{str(current_time.microsecond)}.png", "wb") as fh:
                fh.write(base64.decodebytes(toByte(b_64_image)))
            path_image = toByte(
                f"C:/users/vagrant/connect_device/assets/{str(current_time.microsecond)}.png")
        else:
            with open(f"/home/vagrant/assets/{str(current_time.microsecond)}.png", "wb") as fh:
                fh.write(base64.decodebytes(toByte(b_64_image)))
            path_image = toByte(
                f"/home/vagrant/assets/{str(current_time.microsecond)}.png")
        func = self.dll.DLL_instance.ImprimeImagem
        func.restype = c_int
        func.argtypes = [c_char_p]
        return func(path_image)
