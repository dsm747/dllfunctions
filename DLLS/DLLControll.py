from ctypes import *
import platform
import os


class DLL:
    def __init__(self, DLL_name):
        self.DLL_instance = None

        if DLL_name is None or DLL_name == "":
            print("Invalid DLL name")
        else:
            DLL_path = DLL_name
        if os.path.isfile(DLL_path):
            [os_architecture, os_name] = platform.architecture()
            try:
                if 'Windows' in os_name:
                    self.DLL_instance = WinDLL(DLL_path)
                else:
                    self.DLL_instance = CDLL(DLL_path)
            except ArgumentError:
                print('Wasn\'t possible to open de DLL file: ' + DLL_path)
        else:
            print("The file \"" + DLL_path + "\" wasn\'t founded")
