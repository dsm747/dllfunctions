import inspect


def command_block(name, version, device, message, category, title="",  options=None):
    if options is None:
        options = []

    def inner(func):
        def function_wrapper():
            list_params = []
            list_type = []
            for i in inspect.signature(func).parameters.keys():
                if i != 'self':
                    list_params.append(i)
                    list_type.append(
                        str(inspect.signature(func).parameters[i].annotation))

            create_json = {"name_function": name, "name_code_function": func.__name__,
                           "version": version,
                           "device": device,
                           "title": title,
                           "message": message,
                           "params": list_params,
                           "category": category,
                           "options": options
                           }

            return create_json

        function_wrapper.unwrapped = func

        return function_wrapper

    return inner
