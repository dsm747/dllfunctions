import sys
import json
from types import SimpleNamespace as Namespace
from DLLS.DLLFunctions import E1
import os

def run_test_cases_E1(arrayCases):
    e1 = E1()
    func = getattr(e1, "FechaConexao")
    func.unwrapped(e1)
    for test in arrayCases:
        func = getattr(e1, test.name_function)
        print(func.unwrapped(e1, *test.parameter))


if __name__ == "__main__":
    json_str = sys.argv[1]
    if os.name == 'nt':
        out_file = open("C://Users//vagrant//" + json_str, "r")
    else:
        out_file = open("/home/vagrant/" + json_str, "r")
    json_dict = json.loads(out_file.read(), object_hook=lambda d: Namespace(**d))
    run_test_cases_E1(json_dict.json_user.test_cases)
